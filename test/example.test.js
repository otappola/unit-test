const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe ("Our first unit tests", () => {
    before(() => {
        // initialization
        // create objects... etc..
        console.log("Initializing tests.");
    });
    it("Can add 1 and 2 together", () => {
        // Tests
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3 for some reason?");
    });
    it("Can devide 8 by 0", () => {
        expect(mylib.divide(8, 0)).equal(false, "Divide by 0 is inpossible");
    });
    after(() => {
        // Cleanup
        // For example: shutdown the Express server.
        console.log("Testing completed!")
    });
});